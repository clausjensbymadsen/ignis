# Ignis
Ignis is an open-source cross-platform photo editing software.

## Description
Ignis is designed to be fast and responsive while also being capable.

## Background
The project was conceived as a proof of concept of several claims:
- Modern hardware is blazingly fast, and there are no good reason why most software is as slow as it is.
- It is quite feasible to write a new, modern piece of software from scratch in C.
- It is quite feasible not to rely heavily on 3rd party libraries.

## Contributing
Ignis is not open to code contributions at the moment. When the project is mature enough, this will change.

Other kinds of contribution are welcome though, like bug reports and feature requests.

Read [CONTRIBUTING](CONTRIBUTING.md) for more information.

## License
Ignis is licensed via the GNU General Public License version 2.

The licensing terms can be found in the [LICENSE](LICENSE) file.

## Project status
The project is at its infancy.

The application is not yet usable and the fundamental structure of the code is not yet decided.
