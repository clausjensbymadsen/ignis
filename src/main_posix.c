#include "common/common.c"
#include "common/annotation.c"
#include "common/emit.c"
#include "std/assert.c"
#include "common/memory.c"
#include "std/heap.c"
#include "common/arena.c"
#include "common/string_ascii.c"
#include "common/string_ucs2.c"
#include "common/string_utf8.c"
#include "common/string_builder.c"

#include "model/model.c"

int main(void)
{
  return 0;
}
