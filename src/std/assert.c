#ifndef STD_ASSERT_C
#define STD_ASSERT_C

#include "../common/assert.c"

#define assert_report_failure(message, file, line) \
  do { \
    fprintf(stderr, "Assertion failure: %s:%u: %s\n", (file), (line), (message)); \
    abort(); \
  } while (0);

#endif
