#ifndef STD_HEAP_C
#define STD_HEAP_C

#include "../common/heap.c"

#include <string.h>

static bool heap_init_impl(void)
{
  return true;
}

static void heap_release_impl(void)
{
}

static void* heap_allocate_impl(size_t size)
{
  assert_nonzero(size);

  return malloc(size);
}

static void* heap_allocate_zero_impl(size_t size)
{
  assert_nonzero(size);

  return calloc(1, size);
}

static void* heap_reallocate_impl(void* allocation, size_t size)
{
  assert_nonzero(size);

  return realloc(allocation, size);
}

static void* heap_reallocate_zero_impl(void* allocation, size_t size)
{
  assert_nonzero(size);

  // TODO: Find a fix
  //
  // If standard library realloc expands the allocation, the contents of the
  // expanded memory is undefined. There does not seem to be a way to zero the
  // expanded memory.
  return realloc(allocation, size);
}

static void heap_deallocate_impl(void* allocation)
{
  assert_nonnull(allocation);

  free(allocation);
}

#endif
