#if !defined(_WIN32)
  #error This main file is for the Win32 version of the application.
#endif

#if !defined(_MSC_VER)
  #error The Win32 version must be compiled with MSVC.
#endif

#include "common/common.c"
#include "common/annotation.c"
#include "common/emit.c"
#include "view/win32/assert.c"
#include "common/memory.c"
#include "view/win32/heap.c"
#include "common/arena.c"
#include "common/string_ascii.c"
#include "common/string_ucs2.c"
#include "common/string_utf8.c"
#include "common/string_builder.c"
#include "view/win32/benchmark.c"
#include "view/win32/io.c"

#include "model/model.c"

HINSTANCE instance;
HWND main_window;
ATOM new_image_window_class;
HFONT gui_font;

#define GUI_PADDING 8

#include "view/win32/window_main.c"
#include "view/win32/window_new_image.c"

int main(void)
{
  int exit_code = EXIT_FAILURE;

  emit_info("Initializing application.");

  /* Initialize COM objects to be apartment-threaded, since we will only be
   * using COM objects for GUI objects.
   */
  HRESULT result = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_SPEED_OVER_MEMORY);
  if (FAILED(result)) goto failure_co_initialize;

  instance = GetModuleHandle(NULL);
  if (instance == NULL) goto failure_get_instance;

  if (!heap_init()) goto failure_heap_init;
  if (!arena_init(&scratch_arena, 1024*1024)) goto failure_arena_init;
  #if defined(BENCHMARK)
    if (!benchmark_init()) goto failure_benchmark_init;
  #endif

  /* Initialize the modern common controls, so we don't get the Win95-looking
   * controls. This depends on the manifest file being embedded as a resource
   * in the executable.
   */
  {
    INITCOMMONCONTROLSEX comctl = {0};
    comctl.dwSize = sizeof(comctl);
    comctl.dwICC = ICC_STANDARD_CLASSES;
    if (InitCommonControlsEx(&comctl) == FALSE) goto failure_common_controls;
  }
  
  {
    // Retrieve the GUI font
    NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(ncm);
    BOOL succeeded = SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(ncm), &ncm, 0);
    if (!succeeded) goto failure_spi;

    gui_font = CreateFontIndirect(&ncm.lfMessageFont);
    if (gui_font == NULL) goto failure_create_font;
  }

  ATOM main_window_class;
  {
    WNDCLASSEX window_class = {0};
    window_class.cbSize = sizeof(window_class);
    window_class.hInstance = instance;
    window_class.hCursor = LoadCursor(NULL, IDC_ARROW);
    window_class.hbrBackground = (HBRUSH)(COLOR_3DFACE + 1);
    window_class.lpfnWndProc = main_window_callback;
    window_class.lpszClassName = TEXT("main_window");
    main_window_class = RegisterClassEx(&window_class);
    if (main_window_class == 0) goto failure_register_main_window_class;

    window_class.lpfnWndProc = new_image_window_callback;
    window_class.lpszClassName = TEXT("new_image_window");
    new_image_window_class = RegisterClassEx(&window_class);
    if (new_image_window_class == 0) goto failure_register_new_image_window_class;
  }

  main_window = CreateWindowEx(WS_EX_APPWINDOW,
    MAKEINTATOM(main_window_class), TEXT(MAIN_WINDOW_TITLE), WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
    NULL, NULL, instance, NULL);
  if (main_window == NULL) goto failure_create_window;

  // TODO: The accelerator code should be made configurable.
  #define ACCELERATOR_TABLE_SIZE 3
  ACCEL accelerator_table[ACCELERATOR_TABLE_SIZE] = {
    {FVIRTKEY | FCONTROL, 'N', MENU_FILE_NEW},
    {FVIRTKEY | FCONTROL, 'O', MENU_FILE_OPEN},
    {FVIRTKEY | FCONTROL, 'S', MENU_FILE_SAVE},
  };
  HACCEL accelerator_handle = CreateAcceleratorTable(accelerator_table, ACCELERATOR_TABLE_SIZE);
  if (accelerator_handle == NULL) goto failure_create_accelerator_table;

  {
    STARTUPINFO startup_info;
    GetStartupInfo(&startup_info);
    if (startup_info.dwFlags & STARTF_USESHOWWINDOW)
      ShowWindow(main_window, startup_info.wShowWindow);
    else
      ShowWindow(main_window, SW_SHOWDEFAULT);
  }

  emit_info("Entering main loop.");
  MSG message;
  while (GetMessage(&message, NULL, 0, 0))
  {
    if (!TranslateAccelerator(main_window, accelerator_handle, &message))
    {
      TranslateMessage(&message);
      DispatchMessage(&message);
    }
    arena_clear(&scratch_arena);
  }

  exit_code = (int)message.wParam;

  DestroyAcceleratorTable(accelerator_handle);
  failure_create_accelerator_table:

  // Main window auto-destroys itself
  failure_create_window:

  UnregisterClass(MAKEINTATOM(new_image_window_class), instance);
  failure_register_new_image_window_class:

  UnregisterClass(MAKEINTATOM(main_window_class), instance);
  failure_register_main_window_class:

  failure_create_font:

  failure_spi:

  failure_common_controls:

  #if defined(BENCHMARK)
    benchmark_release();
    failure_benchmark_init:
  #endif

  arena_release(&scratch_arena);
  failure_arena_init:

  heap_release();
  failure_heap_init:

  failure_get_instance:

  CoUninitialize();
  failure_co_initialize:

  return exit_code;
}
