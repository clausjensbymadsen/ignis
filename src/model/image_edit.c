#ifndef MODEL_IMAGE_EDIT_C
#define MODEL_IMAGE_EDIT_C

#define IMAGE_EDIT_INCREMENTATION 256

static bool image_edit_add(struct image* image, image_edit_operation operation, void* data)
{
  assert_nonnull(image);
 
  // Expand dynamic array if necessary
  if (image->edits == NULL)
  {
    image->edits = heap_allocate(IMAGE_EDIT_INCREMENTATION * sizeof(struct image_edit));
    image->edit_count_allocated = IMAGE_EDIT_INCREMENTATION;
  }
  if (image->edit_count >= image->edit_count_allocated)
  {
    void* temp = heap_reallocate(image->edits, (image->edit_count_allocated + IMAGE_EDIT_INCREMENTATION) * sizeof(struct image_edit));
    if (temp == NULL) return false;

    image->edits = temp;
    image->edit_count_allocated += IMAGE_EDIT_INCREMENTATION;
  }

  // Register edit
  struct image_edit* edit = &image->edits[image->edit_count++];
  edit->operation = operation;
  edit->data = data;

  return true;
}

static inline size_t image_edit_count(struct image* image)
{
  assert_nonnull(image);

  return image->edit_count;
}

#if defined(TEST)
SPECIFICATION(image_edit)
{
  heap_init();

  SCENARIO("A newly created image has exactly 1 image edit.")
  {
    struct image* image = image_create_blank(100, 150, image_format_rgb);

    CHECK(image_edit_count(image) == 1);

    image_destroy(image);
  }

  SCENARIO("A newly loaded image has exactly 1 image edit.")
  {
    uint32_t* pixels = heap_allocate(6 * sizeof(uint32_t));
    pixels[0] = 0xFFFFFFFF;
    pixels[1] = 0x80808080;
    pixels[2] = 0x00000000;
    pixels[3] = 0xFF000000;
    pixels[4] = 0x00FF0000;
    pixels[5] = 0x0000FF00;

    struct image* image = image_load(3, 2, pixels);

    CHECK(image_edit_count(image) == 1);

    image_destroy(image);
  }

  heap_release();
}
#endif

#endif