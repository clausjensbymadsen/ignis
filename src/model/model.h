#ifndef MODEL_H
#define MODEL_H

struct image;
struct layer;
struct image_edit;
struct render;

/* The list of images in the application is implemented as a linked list.
 *
 * For normal use of the application, this list is expected to be short and
 * switching between images to be a rare use case. Therefore the data
 * structure of this list in not important.
 */
extern struct image* images;
extern struct image* current_image;

// The image struct represent an image ready for editing.
struct image
{
  struct image* next;
  struct image_edit* edits;
  size_t edit_count;
  size_t edit_count_allocated;
  char* filename;
  bool modified;
};

// The image_edit struct represents an edit done to an image.
typedef bool (*image_edit_operation)(struct render* render, void* data);
struct image_edit
{
  image_edit_operation operation;
  void* data;
};

/* A render struct contains the dimensions and pixel data of a render of the
 * image. Unlike the image struct it does not contain metadata like file name,
 * etc.
 *
 * This is primarily used for rendering an image for display on the screen.
 */
struct render
{
  uint32_t width;
  uint32_t height;
  uint32_t* pixels;
};

enum image_format {image_format_rgb, image_format_cmyk};
typedef enum image_format image_format;

static struct image* image_create_blank(unsigned int width, unsigned int height, image_format format);
static struct image* image_create_from_buffer(unsigned int width, unsigned int height, uint32_t* pixels, image_format format);
static struct image* image_create_from_file(msvc_in const char* filename);
static void          image_destroy(msvc_in struct image* image);
static unsigned int  image_count(void);

static inline bool   image_has_filename(msvc_in struct image* image);
static inline char*  image_filename(msvc_in struct image* image);
static inline bool   image_modified(msvc_in struct image* image);

static void          image_undo(msvc_in struct image* image);
static void          image_redo(msvc_in struct image* image);

// Edit the image by adding image edits. These are automatically supported
// by undo/redo.
static bool          image_edit_add(struct image* image, image_edit_operation operation, void* data);
static inline size_t image_edit_count(struct image* image);

// TODO: Add interface for handling multiple color modes.

static bool          image_render(msvc_in struct image* image, msvc_out struct render* r);

static void          layer_create(msvc_in struct image* image);
static void          layer_destroy(msvc_in struct image* image, unsigned int index);
static unsigned int  layer_count(msvc_in struct image* image);
static unsigned int  layer_current(msvc_in struct image* image);

static void          layer_show(msvc_in struct image* image, unsigned int index);
static void          layer_hide(msvc_in struct image* image, unsigned int index);

#endif