#ifndef MODEL_IMAGE_EVENT_LOAD_IMAGE_C
#define MODEL_IMAGE_EVENT_LOAD_IMAGE_C

struct render_load_image_data
{
  unsigned int width;
  unsigned int height;
  uint32_t* pixels;
};

static bool render_load_image(struct render* render, void* data)
{
  assert_nonnull(render);
  assert_nonnull(data);

  struct render_load_image_data* load_image_data = (struct render_load_image_data*)data;

  uint32_t* pixels = heap_allocate(load_image_data->width * load_image_data->height * 4);
  if (pixels == NULL) return false;
  memory_copy(load_image_data->pixels, pixels, load_image_data->width * load_image_data->height * 4);

  if (render->pixels != NULL) heap_deallocate(render->pixels);
  render->width = load_image_data->width;
  render->height = load_image_data->height;
  render->pixels = pixels;
  return true;
}

#endif
