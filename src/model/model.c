#include "model.h"

struct image* images = NULL;
struct image* current_image = NULL;

#include "image_event_new_image.c"
#include "image_event_load_image.c"

#include "image.c"
#include "layer.c"
#include "image_edit.c"
