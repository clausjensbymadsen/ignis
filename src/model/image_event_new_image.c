#ifndef MODEL_IMAGE_EVENT_NEW_IMAGE_C
#define MODEL_IMAGE_EVENT_NEW_IMAGE_C

struct render_new_image_data
{
  unsigned int width;
  unsigned int height;
};

static bool render_new_image(struct render* render, void* data)
{
  assert_nonnull(render);
  assert_nonnull(data);

  struct render_new_image_data* new_image_data = (struct render_new_image_data*)data;

  uint32_t* pixels = heap_allocate(new_image_data->width * new_image_data->height * 4);
  if (pixels == NULL) return false;
  memory_set(pixels, new_image_data->width * new_image_data->height * 4, 255);

  if (render->pixels != NULL) heap_deallocate(render->pixels);
  render->width = new_image_data->width;
  render->height = new_image_data->height;
  render->pixels = pixels;
  return true;
}

#endif
