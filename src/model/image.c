#ifndef MODEL_IMAGE_C
#define MODEL_IMAGE_C

static unsigned int image_count(void)
{
  if (images == NULL) return 0;

  struct image* image = images;
  unsigned int count = 1;
  while ((image = image->next)) count++;
  return count;
}

static bool image_render(msvc_in struct image* image, msvc_out struct render* r)
{
  assert_nonnull(image);
  assert_nonnull(r);

  r->width = 0;
  r->height = 0;
  r->pixels = NULL;

  for (unsigned int i = 0; i < image_edit_count(image); ++i)
  {
    image_edit_operation op = image->edits[i].operation;
    if (op != NULL)
    {
      if (!op(r, image->edits[i].data)) return false;
    }
  }
  return true;
}

static struct image* image_create_blank(unsigned int width, unsigned int height, image_format format)
{
  assert_positive(width);
  assert_positive(height);

  // Create image
  struct image* new_image = heap_allocate_zero(sizeof(struct image));
  if (!new_image) goto failure_memory_allocate;

  new_image->next = NULL;
  new_image->edits = NULL;
  new_image->filename = NULL;

  struct render_new_image_data* data = heap_allocate(sizeof(struct render_new_image_data));
  if (data == NULL) goto failure_memory_allocate_new_image_data;
  data->width = width;
  data->height = height;
  image_edit_add(new_image, render_new_image, data);

  // Insert into linked list
  if (images == NULL)
    images = new_image;
  else
  {
    new_image->next = images;
    images = new_image;
  }

  current_image = new_image;
  return new_image;

  failure_memory_allocate_new_image_data:

  heap_deallocate(new_image);
  failure_memory_allocate:

  return NULL;
}

static struct image* image_load(unsigned int width, unsigned int height, uint32_t* pixels)
{
  assert_positive(width);
  assert_positive(height);
  assert_nonnull(pixels);

  // Create image
  struct image* new_image = heap_allocate_zero(sizeof(struct image));
  if (!new_image) goto failure_memory_allocate;

  new_image->next = NULL;
  new_image->edits = NULL;
  new_image->filename = NULL;

  struct render_load_image_data* data = heap_allocate(sizeof(struct render_load_image_data));
  if (data == NULL) goto failure_memory_allocate_load_image_data;
  data->width = width;
  data->height = height;
  data->pixels = pixels;
  image_edit_add(new_image, render_load_image, data);

  // Insert into linked list
  if (images == NULL)
    images = new_image;
  else
  {
    new_image->next = images;
    images = new_image;
  }

  return new_image;

  failure_memory_allocate_load_image_data:

  heap_deallocate(new_image);
  failure_memory_allocate:

  return NULL;
}

static void image_destroy(msvc_in struct image* image)
{
  assert_nonnull(image);

  // Remove from linked list
  if (images == image)
  {
    images = image->next;
    if (current_image == image) current_image = image->next;
  }
  else
  {
    struct image* i = images;
    struct image* previous = images;
    while ((i = i->next))
    {
      if (i == image)
      {
        previous->next = i->next;
        break;
        if (current_image == image) current_image = previous;
      }
      previous = i;
    }
  }

  // Change current image
  if (image == current_image)
  {
    if (image->next != NULL)
      current_image = image->next;
    else
      current_image = images;
  }

  // Destroy image
  if (image->edits != NULL)
  {
    for (unsigned int i = 0; i < image->edit_count; ++i)
    {
      if (image->edits[i].operation == render_load_image)
        heap_deallocate(((struct render_load_image_data*)image->edits[i].data)->pixels);

      assert_nonnull(image->edits[i].data);
      heap_deallocate(image->edits[i].data);
      #if defined(DEBUG)
        image->edits[i].data = NULL;
      #endif
    }

    heap_deallocate(image->edits);
    #if defined(DEBUG)
      image->edits = NULL;
    #endif
  }
  heap_deallocate(image);
}

static inline bool image_has_filename(msvc_in struct image* image)
{
  assert_nonnull(image);

  return image->filename != NULL;
}

static inline char* image_filename(msvc_in struct image* image)
{
  assert_nonnull(image);

  return image->filename;
}

static inline bool image_modified(msvc_in struct image* image)
{
  return image->modified;
}

#if defined(TEST)
SPECIFICATION(image)
{
  heap_init();

  SCENARIO("Creating an image does not return NULL.")
  {
    struct image* image = image_create_blank(100, 150, image_format_rgb);

    CHECK(image != NULL);

    image_destroy(image);
  }

  SCENARIO("When there are no images, then the image count is 0.")
  {
    CHECK(image_count() == 0);
  }

  SCENARIO("When creating an image, then the image count increases.");
  {
    int pre_count = image_count();
    struct image* image = image_create_blank(200, 100, image_format_rgb);
    int post_count = image_count();
    image_destroy(image);

    CHECK(post_count > pre_count);
  }

  SCENARIO("When destroying an image, then the image count decreases.")
  {
    struct image* image = image_create_blank(200, 100, image_format_rgb);
    int pre_count = image_count();
    image_destroy(image);
    int post_count = image_count();

    CHECK(post_count < pre_count);
  }

  SCENARIO("When creating two images, then the image count increases by two.");
  {
    int pre_count = image_count();
    struct image* image1 = image_create_blank(200, 100, image_format_rgb);
    struct image* image2 = image_create_blank(300, 200, image_format_rgb);
    int post_count = image_count();
    image_destroy(image1);
    image_destroy(image2);

    CHECK(post_count = pre_count + 2);
  }

  SCENARIO("When creating an image, a pointer is returned.")
  {
    struct image* image = image_create_blank(50, 75, image_format_rgb);

    CHECK(image != NULL);

    image_destroy(image);
  }

  SCENARIO("A newly created image has no file name.")
  {
    struct image* image = image_create_blank(150, 200, image_format_rgb);

    CHECK(!image_has_filename(image));

    image_destroy(image);
  }

  SCENARIO("A newly created image is unmodified.")
  {
    struct image* image = image_create_blank(150, 200, image_format_rgb);

    CHECK(!image_modified(image));

    image_destroy(image);
  }

  SCENARIO("When creating three images, they are put into linked list in reverse order.")
  {
    // TODO: Consider relaxing this test to accept any order, because order isn't really imporant for this.
    struct image* image1 = image_create_blank(100, 200, image_format_rgb);
    struct image* image2 = image_create_blank(200, 100, image_format_rgb);
    struct image* image3 = image_create_blank(50, 50, image_format_rgb);

    CHECK(images == image3);
    CHECK(images->next == image2);
    CHECK(images->next->next == image1);
    CHECK(images->next->next->next == NULL);

    image_destroy(image1);
    image_destroy(image2);
    image_destroy(image3);
  }

  SCENARIO("When creating three images and destroying them in any order, the linked list is empty.")
  {
    struct image* image1 = image_create_blank(100, 200, image_format_rgb);
    struct image* image2 = image_create_blank(200, 100, image_format_rgb);
    struct image* image3 = image_create_blank(50, 50, image_format_rgb);
    image_destroy(image2);
    image_destroy(image1);
    image_destroy(image3);

    CHECK(images == NULL);
  }

  SCENARIO("If there are exactly 1 image, that is also the current image.")
  {
    struct image* image = image_create_blank(100, 150, image_format_rgb);

    CHECK(current_image == image);

    image_destroy(image);
  }

  SCENARIO("If there are exactly 0 images, then there are no current image.")
  {
    CHECK(current_image == NULL);
  }

  SCENARIO("If creating an additional image, then current image is changed to the new image.")
  {
    struct image* image1 = image_create_blank(100, 150, image_format_rgb);
    struct image* image2 = image_create_blank(100, 150, image_format_rgb);

    CHECK(current_image == image2);

    image_destroy(image1);
    image_destroy(image2);
  }

  heap_release();
}
#endif

#endif
