#ifndef WIN32_HEAP_C
#define WIN32_HEAP_C

#include "../../common/heap.c"

HANDLE process_heap;

static bool heap_init_impl(void)
{
  process_heap = GetProcessHeap();
  return process_heap != NULL;
}

static void heap_release_impl(void)
{
}

static void* heap_allocate_impl(size_t size)
{
  assert_positive(size);

  return HeapAlloc(process_heap, 0, size);
}

static void* heap_allocate_zero_impl(size_t size)
{
  assert_positive(size);

  return HeapAlloc(process_heap, HEAP_ZERO_MEMORY, size);
}

static void* heap_reallocate_impl(void* allocation, size_t size)
{
  assert_positive(size);

  if (allocation == NULL) return HeapAlloc(process_heap, 0, size);
  return HeapReAlloc(process_heap, 0, allocation, size);
}

static void* heap_reallocate_zero_impl(void* allocation, size_t size)
{
  assert_positive(size);

  if (allocation == NULL) return HeapAlloc(process_heap, HEAP_ZERO_MEMORY, size);
  return HeapReAlloc(process_heap, HEAP_ZERO_MEMORY, allocation, size);
}

static void heap_deallocate_impl(void* allocation)
{
  assert_nonnull(allocation);

  HeapFree(process_heap, 0, allocation);
}

#endif
