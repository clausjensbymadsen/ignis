#ifndef WIN32_WINDOW_NEW_IMAGE_C
#define WIN32_WINDOW_NEW_IMAGE_C

#define GUI_NEW_IMAGE_BUTTON_WIDTH 100
#define GUI_NEW_IMAGE_BUTTON_HEIGHT 30
#define GUI_NEW_IMAGE_EDIT_WIDTH 100
#define GUI_NEW_IMAGE_EDIT_HEIGHT 30

#define ID_NEW_IMAGE_BUTTON_OK 1
#define ID_NEW_IMAGE_BUTTON_CANCEL 2

HWND new_image_button_ok;
HWND new_image_button_cancel;
HWND new_image_edit_width;
HWND new_image_edit_height;

LRESULT CALLBACK new_image_window_callback(_In_ HWND window_handle, _In_ UINT message, _In_ WPARAM wparam, _In_ LPARAM lparam)
{
  switch(message)
  {
    case WM_CREATE:
    {
      new_image_button_ok = CreateWindowEx(0,
        TEXT("BUTTON"), TEXT("OK"), WS_VISIBLE | WS_CHILD,
        10, 10, GUI_NEW_IMAGE_BUTTON_WIDTH, GUI_NEW_IMAGE_BUTTON_HEIGHT,
        window_handle, (HMENU)ID_NEW_IMAGE_BUTTON_OK, instance, NULL);
      if (new_image_button_ok == NULL) return -1;
      SendMessage(new_image_button_ok, WM_SETFONT, (WPARAM)gui_font, MAKELPARAM(TRUE, 0));
      
      new_image_button_cancel = CreateWindowEx(0,
        TEXT("BUTTON"), TEXT("Cancel"), WS_VISIBLE | WS_CHILD,
        120, 10, GUI_NEW_IMAGE_BUTTON_WIDTH, GUI_NEW_IMAGE_BUTTON_HEIGHT,
        window_handle, (HMENU)ID_NEW_IMAGE_BUTTON_CANCEL, instance, NULL);
      if (new_image_button_cancel == NULL) return -1;
      SendMessage(new_image_button_cancel, WM_SETFONT, (WPARAM)gui_font, MAKELPARAM(TRUE, 0));
      
      new_image_edit_width = CreateWindowEx(0,
        TEXT("EDIT"), TEXT("800"), WS_VISIBLE | WS_CHILD,
        10, 50, GUI_NEW_IMAGE_EDIT_WIDTH, GUI_NEW_IMAGE_EDIT_HEIGHT,
        window_handle, NULL, instance, NULL);
      if (new_image_edit_width == NULL) return -1;
      SendMessage(new_image_edit_width, WM_SETFONT, (WPARAM)gui_font, MAKELPARAM(TRUE, 0));
      
      new_image_edit_height = CreateWindowEx(0,
        TEXT("EDIT"), TEXT("600"), WS_VISIBLE | WS_CHILD,
        10, 100, GUI_NEW_IMAGE_EDIT_WIDTH, GUI_NEW_IMAGE_EDIT_HEIGHT,
        window_handle, NULL, instance, NULL);
      if (new_image_edit_height == NULL) return -1;
      SendMessage(new_image_edit_height, WM_SETFONT, (WPARAM)gui_font, MAKELPARAM(TRUE, 0));
        
      return 0;
    }
    case WM_DESTROY:
    {
      ShowWindow(window_handle, SW_HIDE);
      SetActiveWindow(main_window);
      EnableWindow(main_window, TRUE);
      return 0;
    }
    case WM_SIZE:
    {
      DWORD client_height = HIWORD(lparam);
      
      SetWindowPos(new_image_button_ok, NULL,
        GUI_PADDING, client_height - GUI_NEW_IMAGE_BUTTON_HEIGHT - GUI_PADDING, 0, 0,
        SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);

      SetWindowPos(new_image_button_cancel, NULL,
        GUI_PADDING*2 + GUI_NEW_IMAGE_BUTTON_WIDTH, client_height - GUI_NEW_IMAGE_BUTTON_HEIGHT - GUI_PADDING, 0, 0,
        SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);

      SetWindowPos(new_image_edit_width, NULL,
        GUI_PADDING, GUI_PADDING, 0, 0,
        SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
        
      SetWindowPos(new_image_edit_height, NULL,
        GUI_PADDING, GUI_PADDING*2 + GUI_NEW_IMAGE_EDIT_HEIGHT, 0, 0,
        SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
        
      return 0;
    }
    case WM_COMMAND:
    {
      WORD id = LOWORD(wparam);
      if (id == ID_NEW_IMAGE_BUTTON_OK)
      {
        // TODO: Get values from edits
        unsigned int width = 500;
        unsigned int height = 400;

        DestroyWindow(window_handle);

        image_create_blank(width, height, image_format_rgb);
        update_main_window();
      }
      if (id == ID_NEW_IMAGE_BUTTON_CANCEL)
      {
        DestroyWindow(window_handle);
      }
      return 0;
    }
    default:
      return DefWindowProc(window_handle, message, wparam, lparam);
  }
}

#endif
