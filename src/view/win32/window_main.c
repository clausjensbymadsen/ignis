#ifndef WIN32_WINDOW_MAIN_C
#define WIN32_WINDOW_MAIN_C

#if defined(DEBUG)
  #define MAIN_WINDOW_TITLE "Ignis (DEBUG BUILD)"
#else
  #define MAIN_WINDOW_TITLE "Ignis"
#endif

HWND toolbar;
HMENU file_menu;

#define MENU_FILE       1
#define MENU_FILE_NEW   100
#define MENU_FILE_OPEN  101
#define MENU_FILE_SAVE  102
#define MENU_FILE_CLOSE 103
#define MENU_FILE_QUIT  104

static void update_main_window(void)
{
  // Update window title
  if (current_image)
  {
    TCHAR* title = string_builder_append(TEXT(MAIN_WINDOW_TITLE));
    string_builder_append(TEXT(" - "));
    if (image_has_filename(current_image))
      string_builder_append(image_filename(current_image));
    else
      string_builder_append(TEXT("(untitled)"));
    if (image_modified(current_image))
      string_builder_append(TEXT('*'));
    string_builder_append(TEXT('\0'));

    SetWindowText(main_window, title);
  }
  else
    SetWindowText(main_window, TEXT(MAIN_WINDOW_TITLE));

  // Update menu
  bool image_open = current_image != NULL;
  EnableMenuItem(file_menu, MENU_FILE_SAVE, image_open ? MF_ENABLED : MF_GRAYED);
  EnableMenuItem(file_menu, MENU_FILE_CLOSE, image_open ? MF_ENABLED : MF_GRAYED);

  // Repaint
  InvalidateRect(main_window, NULL, TRUE);
}

LRESULT CALLBACK main_window_callback(_In_ HWND window_handle, _In_ UINT message, _In_ WPARAM wparam, _In_ LPARAM lparam)
{
  switch(message)
  {
    case WM_COMMAND:
    {
      if ((lparam == 0) || (lparam == (LPARAM)toolbar))
      {
        switch (LOWORD(wparam))
        {
          case MENU_FILE_NEW:
          {
            RECT rect;
            GetWindowRect(window_handle, &rect);
            int parent_width = rect.right - rect.left;
            int parent_height = rect.bottom - rect.top;
            int child_width = 500;
            int child_height = 400;
          
            EnableWindow(window_handle, FALSE);
            HWND new_image_window = CreateWindowEx(WS_EX_DLGMODALFRAME | WS_EX_TOPMOST,
              MAKEINTATOM(new_image_window_class), TEXT("New image"), WS_POPUPWINDOW | WS_CAPTION | WS_DLGFRAME | WS_VISIBLE,
              rect.left + (parent_width - child_width) / 2, rect.top + (parent_height - child_height) / 2, child_width, child_height,
              window_handle, NULL, instance, NULL);
            if (new_image_window == NULL)
            {
              MessageBox(window_handle, TEXT("Failed to create window."), TEXT("New image"), MB_OK | MB_ICONERROR);
              return -1;
            }
            
            return 0;
          }
          case MENU_FILE_OPEN:
          {
            LRESULT return_code = 1;
            IFileOpenDialog* file_open_dialog;
            HRESULT result = CoCreateInstance(&CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, &IID_IFileOpenDialog, &file_open_dialog);
            if (FAILED(result)) goto failure_open_create_instance;

            DWORD flags;
            result = file_open_dialog->lpVtbl->GetOptions(file_open_dialog, &flags);
            if (FAILED(result)) goto failure_open_get_options;

            result = file_open_dialog->lpVtbl->SetOptions(file_open_dialog, flags | FOS_FORCEFILESYSTEM | FOS_FILEMUSTEXIST | FOS_FORCEPREVIEWPANEON);
            if (FAILED(result)) goto failure_open_set_options;

            result = file_open_dialog->lpVtbl->Show(file_open_dialog, window_handle);
            if (FAILED(result)) goto failure_open_show;

            IShellItem* item;
            result = file_open_dialog->lpVtbl->GetResult(file_open_dialog, &item);
            if (FAILED(result)) goto failure_open_get_result;

            LPWSTR filename;
            result = item->lpVtbl->GetDisplayName(item, 0, &filename);
            if (FAILED(result)) goto failure_open_get_display_name;

            // TODO: Load image from filename
            update_main_window();

            return_code = 0;

            failure_open_get_display_name:

            item->lpVtbl->Release(item);
            failure_open_get_result:

            failure_open_show:

            failure_open_set_options:

            failure_open_get_options:

            file_open_dialog->lpVtbl->Release(file_open_dialog);
            failure_open_create_instance:

            return return_code;
          }
          case MENU_FILE_SAVE:
          {
            LRESULT return_code = 1;
            IFileSaveDialog* file_save_dialog;
            HRESULT result = CoCreateInstance(&CLSID_FileSaveDialog, NULL, CLSCTX_INPROC_SERVER, &IID_IFileSaveDialog, &file_save_dialog);
            if (FAILED(result)) goto failure_save_create_instance;

            DWORD flags;
            result = file_save_dialog->lpVtbl->GetOptions(file_save_dialog, &flags);
            if (FAILED(result)) goto failure_save_get_options;

            result = file_save_dialog->lpVtbl->SetOptions(file_save_dialog, flags | FOS_OVERWRITEPROMPT | FOS_PATHMUSTEXIST | FOS_NOREADONLYRETURN | FOS_NOTESTFILECREATE);
            if (FAILED(result)) goto failure_save_set_options;

            result = file_save_dialog->lpVtbl->Show(file_save_dialog, window_handle);
            if (FAILED(result)) goto failure_save_show;

            IShellItem* item;
            result = file_save_dialog->lpVtbl->GetResult(file_save_dialog, &item);
            if (FAILED(result)) goto failure_save_get_result;

            LPWSTR filename;
            result = item->lpVtbl->GetDisplayName(item, 0, &filename);
            if (FAILED(result)) goto failure_save_get_display_name;

            // TODO: Save image from filename

            return_code = 0;

            failure_save_get_display_name:

            item->lpVtbl->Release(item);
            failure_save_get_result:

            failure_save_show:

            failure_save_set_options:

            failure_save_get_options:

            file_save_dialog->lpVtbl->Release(file_save_dialog);
            failure_save_create_instance:

            return return_code;
          }
          case MENU_FILE_CLOSE:
          {
            if (current_image == NULL) return 0;
            
            image_destroy(current_image);
            update_main_window();
            return 0;
          }
          case MENU_FILE_QUIT:
          {
            PostMessage(window_handle, WM_CLOSE, 0, 0);
            return 0;
          }
          default:
            assert_unreachable();
        }
      }
    }
    case WM_CREATE:
    {
      toolbar = CreateWindowEx(0, TOOLBARCLASSNAME, NULL, WS_CHILD | WS_VISIBLE,
        0, 0, 0, 0,
        window_handle, NULL, instance, NULL);
      if (toolbar == NULL) return -1;

      SendMessage(toolbar, TB_BUTTONSTRUCTSIZE, sizeof(TBBUTTON), 0);

      #define TOOLBAR_BUTTON_COUNT 3
      TBBUTTON buttons[TOOLBAR_BUTTON_COUNT] = {
        {I_IMAGENONE, MENU_FILE_NEW,  TBSTATE_ENABLED, BTNS_AUTOSIZE, {0}, 0, (INT_PTR)TEXT("New")},
        {I_IMAGENONE, MENU_FILE_OPEN, TBSTATE_ENABLED, BTNS_AUTOSIZE, {0}, 0, (INT_PTR)TEXT("Open")},
        {I_IMAGENONE, MENU_FILE_SAVE, TBSTATE_ENABLED, BTNS_AUTOSIZE, {0}, 0, (INT_PTR)TEXT("Save")}
      };
      LRESULT result = SendMessage(toolbar, TB_ADDBUTTONS, TOOLBAR_BUTTON_COUNT, (LPARAM)&buttons);
      if (result == FALSE) return -1;

      SendMessage(toolbar, TB_AUTOSIZE, 0, 0);

      // Programmatically construct the menu, because this should be configurable in the future.
      HMENU main_menu = CreateMenu();
      if (main_menu == NULL) return -1;
      file_menu = CreateMenu();
      if (file_menu == NULL) return -1;

      MENUITEMINFO menu_item_info_separator;
      menu_item_info_separator.cbSize = sizeof(menu_item_info_separator);
      menu_item_info_separator.fMask = MIIM_TYPE;
      menu_item_info_separator.fType = MFT_SEPARATOR;

      MENUITEMINFO menu_item_info;
      menu_item_info.cbSize = sizeof(menu_item_info);
      menu_item_info.fMask = MIIM_ID | MIIM_TYPE | MIIM_SUBMENU;
      menu_item_info.fType = MFT_STRING;
      menu_item_info.wID = MENU_FILE;
      menu_item_info.hSubMenu = file_menu;
      menu_item_info.dwTypeData = TEXT("&File");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(main_menu, 0, FALSE, &menu_item_info)) return -1;

      menu_item_info.fMask = MIIM_ID | MIIM_TYPE;
      menu_item_info.wID = MENU_FILE_NEW;
      menu_item_info.dwTypeData = TEXT("&New\tCtrl+N");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info)) return -1;

      menu_item_info.wID = MENU_FILE_OPEN;
      menu_item_info.dwTypeData = TEXT("&Open\tCtrl+O");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info)) return -1;

      menu_item_info.wID = MENU_FILE_SAVE;
      menu_item_info.dwTypeData = TEXT("&Save\tCtrl+S");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info)) return -1;

      menu_item_info.wID = MENU_FILE_CLOSE;
      menu_item_info.dwTypeData = TEXT("Close");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info)) return -1;

      // TODO: Fix separator inserted into wrong location
      // if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info_separator)) return -1;

      menu_item_info.fMask = MIIM_TYPE | MIIM_ID;
      menu_item_info.wID = MENU_FILE_QUIT;
      menu_item_info.dwTypeData = TEXT("&Quit\tAlt+F4");
      menu_item_info.cch = (UINT)wcslen(menu_item_info.dwTypeData);
      if (!InsertMenuItem(file_menu, 0, FALSE, &menu_item_info)) return -1;
      
      SetMenu(window_handle, main_menu);
      update_main_window();
      return 0;
    }
    case WM_CLOSE:
      bool can_close = true; // TODO: Check if main window can close (unsaved changes, etc.)

      if (can_close) DestroyWindow(window_handle);
      return 0;
    case WM_DESTROY:
      PostQuitMessage(EXIT_SUCCESS);
      while (images) image_destroy(images);
      return 0;
    case WM_PAINT:
      {
        UpdateWindow(toolbar);
        if (current_image == NULL) return 0;

        PAINTSTRUCT ps;
        HDC dc = BeginPaint(window_handle, &ps);
        if (dc == NULL) return 1;

        struct render r;
        if (!image_render(current_image, &r)) return 1;

        BITMAPINFO bitmap_info = {0};
        bitmap_info.bmiHeader.biSize = sizeof(bitmap_info.bmiHeader);
        bitmap_info.bmiHeader.biWidth = r.width;
        bitmap_info.bmiHeader.biHeight = r.height;
        bitmap_info.bmiHeader.biPlanes = 1;
        bitmap_info.bmiHeader.biBitCount = 32;
        bitmap_info.bmiHeader.biCompression = BI_RGB;

        assert_positive(bitmap_info.bmiHeader.biWidth);
        assert_positive(bitmap_info.bmiHeader.biHeight);
        if (!SetDIBitsToDevice(dc, 0, 0, r.width, r.height, 0, 0, 0, r.height, r.pixels, &bitmap_info, DIB_RGB_COLORS)) // TODO: Fix
        {
          DWORD error = GetLastError();
          if (error == 0)
            emit_warning("SetDIBitsToDevice failed: zero scan lines were set.\n");
          else
            emit_error("SetDIBitsToDevice failed: error %lu\n", error);
          heap_deallocate(r.pixels);
          return 1;
        }
        heap_deallocate(r.pixels);
        EndPaint(window_handle, &ps);
        return 0;
      }
    default:
      return DefWindowProc(window_handle, message, wparam, lparam);
  }
}

#endif
