#ifndef WIN32_IO_C
#define WIN32_IO_C

#include "../../common/io.c"

static void* load_file_into_memory_str(const char* filename)
{
  void* result = NULL;
  assert_nonnull(filename);

  HANDLE file = CreateFileA(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
    FILE_FLAG_SEQUENTIAL_SCAN, NULL);
  if (file == INVALID_HANDLE_VALUE) goto failure_open_file;
  
  ULARGE_INTEGER file_size;
  file_size.u.LowPart = GetFileSize(file, &file_size.u.HighPart);
  if (file_size.u.LowPart == INVALID_FILE_SIZE) goto failure_get_file_size;
  if (file_size.u.HighPart != 0) goto failure_file_too_big;

  void* buffer = heap_allocate(file_size.u.LowPart);
  if (buffer == NULL) goto failure_allocate_memory;

  DWORD read;
  BOOL read_result = ReadFile(file, buffer, file_size.u.LowPart, &read, NULL);
  if (read_result == FALSE) goto failure_read_file;
  if (read != file_size.u.LowPart) goto failure_read_file;
  
  result = buffer;
  
  failure_read_file:

  heap_deallocate(buffer);
  failure_allocate_memory:

  failure_file_too_big:

  failure_get_file_size:
  
  CloseHandle(file);
  failure_open_file:

  return result;
}

static void* load_file_into_memory_wstr(const wchar_t* filename)
{
  filename = filename; // TODO: Port from narrow string version
  return NULL;
}

#endif
