#ifndef WIN32_ASSERT_C
#define WIN32_ASSERT_C

#include "../../common/assert.c"

#define assert_report_failure(message, file, line) \
  do { \
    MessageBox(NULL, TEXT(file) TEXT(":") TEXT(#line) TEXT(":\n\n") TEXT(message), TEXT("Assertion failure"), MB_OK | MB_ICONERROR); \
    ExitProcess(EXIT_FAILURE); \
  } while (0);

#endif
