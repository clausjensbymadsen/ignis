#ifndef WIN32_BENCHMARK_C
#define WIN32_BENCHMARK_C

#if defined(BENCHMARK)
LARGE_INTEGER performance_frequency;

static bool benchmark_init(void)
{
  QueryPerformanceFrequency(&performance_frequency);
  return true;
}

static void benchmark_release(void)
{
}

typedef LARGE_INTEGER timestamp;

#define benchmark_start(timestamp_start) do { QueryPerformanceCounter(&timestamp_start); } while(0);
#define benchmark_stop(timestamp_stop) do { QueryPerformanceCounter(&timestamp_stop); } while(0);

static size_t benchmark_span_us(timestamp timestamp_start, timestamp timestamp_stop)
{
  assert_true(timestamp_start.QuadPart < timestamp_stop.QuadPart);

  return ((timestamp_stop.QuadPart - timestamp_start.QuadPart) * 1000000 / performance_frequency.QuadPart);
}

static size_t benchmark_span_ms(timestamp timestamp_start, timestamp timestamp_stop)
{
  return benchmark_span_us(timestamp_start, timestamp_stop) / 1000;
}

static size_t benchmark_span_s(timestamp timestamp_start, timestamp timestamp_stop)
{
  return benchmark_span_us(timestamp_start, timestamp_stop) / 1000000;
}

#endif

#endif
