#ifndef COMMON_ARENA_C
#define COMMON_ARENA_C

struct arena
{
  char* allocation;
  size_t usage;
  #if defined(DEBUG)
    size_t size;
    bool arena_initialized;
  #endif
};
typedef struct arena arena;

/* Default arena for small, miscellaneous, ad hoc allocation needs. Use ONLY
 * in main thread.
 */
arena scratch_arena;

static bool arena_init(msvc_in arena* a, size_t size)
{
  assert_nonnull(a);
  #if defined(DEBUG)
    assert_true(heap_initialized);
  #endif

  #if defined(DEBUG)
    assert_false(a->arena_initialized);
    a->arena_initialized = true;
  #endif

  a->allocation = heap_allocate(size);
  a->usage = 0;
  #if defined(DEBUG)
    a->size = size;
  #endif

  return a->allocation != NULL;
}

static void arena_release(msvc_in arena* a)
{
  assert_nonnull(a);
  #if defined(DEBUG)
    assert_true(a->arena_initialized);
    a->arena_initialized = false;
  #endif

  heap_deallocate(a->allocation);
  #if defined(DEBUG)
    a->allocation = NULL;
    a->usage = 0;
  #endif
}

static inline size_t arena_usage(msvc_in arena* a)
{
 assert_nonnull(a);

 return a->usage;
}

static inline void arena_usage_increase(msvc_in arena* a, msvc_in size_t amount)
{
 assert_nonnull(a);
 assert_true(a->usage <= SIZE_MAX - amount);

 a->usage += amount;
}

static inline void arena_usage_decrease(msvc_in arena* a, msvc_in size_t amount)
{
 assert_nonnull(a);
 assert_true(a->usage >= amount);

 a->usage -= amount;
}

static void* arena_allocate(msvc_in arena* a, msvc_in size_t size)
{
  assert_nonnull(a);
  assert_positive(size);
  #if defined(DEBUG)
    assert_true(a->arena_initialized);
    assert_true(a->usage + size <= a->size);
  #endif

  void* result = a->allocation + a->usage;
  a->usage += size;
  return result;
}

static void* arena_allocate_aligned(msvc_in arena* a, msvc_in size_t size, msvc_in unsigned int alignment)
{
  assert_nonnull(a);
  assert_positive(size);
  #if defined(DEBUG)
    assert_true(a->arena_initialized);
    assert_true(a->usage + size <= a->size);
  #endif

  unsigned int offset = alignment_offset(a->allocation + a->usage, alignment);
  if (offset > 0) a->usage = a->usage - offset + alignment;

  assert_true(aligned(a->allocation + a->usage, alignment));
  return arena_allocate(a, size);
}

static void arena_clear(msvc_in arena* a)
{
  assert_nonnull(a);
  #if defined(DEBUG)
    assert_true(a->arena_initialized);

    memory_set_zero(a->allocation, a->size);
  #endif
  a->usage = 0;
}

#if defined(TEST)
SPECIFICATION(arena)
{
  heap_init();

  SCENARIO("A newly created arena is empty.")
  {
    arena test_arena;
    #if defined(DEBUG)
      test_arena.arena_initialized = false;
    #endif
    arena_init(&test_arena, 4096);

    CHECK(arena_usage(&test_arena) == 0);

    arena_release(&test_arena);
  }

  SCENARIO("When arena-allocating 10 and 15 bytes, 25 bytes total are allocated")
  {
    arena test_arena;
    #if defined(DEBUG)
      test_arena.arena_initialized = false;
    #endif
    arena_init(&test_arena, 4096);
    arena_allocate(&test_arena, 10);
    arena_allocate(&test_arena, 15);

    CHECK(arena_usage(&test_arena) == 25);

    arena_release(&test_arena);
  }

  SCENARIO("Arena becomes empty when cleared")
  {
    arena test_arena;
    #if defined(DEBUG)
      test_arena.arena_initialized = false;
    #endif
    arena_init(&test_arena, 4096);
    arena_allocate(&test_arena, 10);

    arena_clear(&test_arena);

    CHECK(arena_usage(&test_arena) == 0);

    arena_release(&test_arena);
  }

  heap_release();
}
#endif

#endif
