#ifndef COMMON_READER_C
#define COMMON_READER_C

struct reader
{
  char* data;
  size_t index;
};
typedef struct reader reader;

void reader_init(reader* r, void* data)
{
  assert_nonnull(r);
  assert_nonnull(data);

  r->data = data;
  r->index = 0;
}

void reader_release(reader* r)
{
  #if defined(DEBUG)
    r->data = NULL;
  #endif
}

char reader_peek(reader* r)
{
  assert_nonnull(r);

  return r->data[r->index];
}

char reader_read(reader* r)
{
  assert_nonnull(r);

  return r->data[r->index++];
}

#if defined(TEST)
SPECIFICATION(reader)
{
  SCENARIO("Reader is initialized to passed pointer starting at index 0")
  {
    char data[3] = {1, 2, 3};
    reader r;

    reader_init(&r, data);

    CHECK(r.data == data);
    CHECK(r.index == 0);

    reader_release(&r);
  }

  SCENARIO("Reader reads the values 1, 2 and 3 from array {1, 2, 3}")
  {
    char data[3] = {1, 2, 3};
    reader r;
    reader_init(&r, data);

    CHECK(reader_read(&r) == 1);
    CHECK(reader_read(&r) == 2);
    CHECK(reader_read(&r) == 3);

    reader_release(&r);
  }

  SCENARIO("Reader peek does not change the index")
  {
    char data[3] = {1, 2, 3};
    reader r;
    reader_init(&r, data);
    r.index = 1;

    char result = reader_peek(&r);

    CHECK(result == 2);
    CHECK(r.index == 1);

    reader_release(&r);
  }
}
#endif

#endif
