#ifndef COMMON_ANNOTATION_C
#define COMMON_ANNOTATION_C

#if defined(_MSC_VER)
  #include <sal.h>
  #define msvc_in _In_
  #define msvc_out _Out_
  #define msvc_in_out _Inout_

  #define msvc_in_optional _In_opt_
  #define msvc_out_optional _Out_opt_
  #define msvc_in_out_optional _Inout_opt_
#else
  #define msvc_in
  #define msvc_out
  #define msvc_in_out

  #define msvc_in_optional
  #define msvc_out_optional
  #define msvc_in_out_optional
#endif

#if defined(__GNUC__)
  #define gcc_attribute(...) __attribute__((__VA_ARGS__))
#else
  #define gcc_attribute(...)
#endif

#endif
