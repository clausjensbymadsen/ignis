#ifndef COMMON_STRING_ASCII_C
#define COMMON_STRING_ASCII_C

static size_t ascii_length(const char* str)
{
  assert_nonnull(str);
  
  size_t index = 0;
  while (str[index] != '\0') index++;
  return index;
}

#define INT_TO_ASCII_BUFFER_SIZE 30
static char* int_to_ascii_buffer(int64_t value, char* buffer, unsigned int buffer_size)
{
  assert_nonnull(buffer);
  assert_positive(buffer_size);

  buffer[buffer_size - 1] = '\0';

  if (value == 0) {
    buffer[buffer_size - 2] = '0';
    return &buffer[buffer_size - 2];
  }
  else if (value == INT64_MIN) {
    memory_copy("-9223372036854775808", buffer, 21); // TODO: Handle corner case more elegantly
    return buffer;
  }
  else {
    int64_t abs_value = value < 0 ? -value : value;
    size_t i = buffer_size - 2;
    while (abs_value > 0) {
      buffer[i--] = (abs_value % 10) + '0';
      abs_value /= 10;
    }

    if (value < 0) buffer[i--] = '-';

    return &buffer[i+1];
  }
}

#define UINT_TO_ASCII_BUFFER_SIZE 30
static char* uint_to_ascii_buffer(uint64_t value, char* buffer, unsigned int buffer_size)
{
  // TODO: Implement
  return int_to_ascii_buffer((uint64_t)value, buffer, buffer_size);
}

static inline char* int_to_ascii(int64_t value)
{
  return int_to_ascii_buffer(value, arena_allocate(&scratch_arena, INT_TO_ASCII_BUFFER_SIZE), INT_TO_ASCII_BUFFER_SIZE);
}

static unsigned int int_length_as_ascii(int64_t value)
{
  unsigned int result = 1;
  if (value < 0)
  {
    ++result;
    while (value < -9)
    {
      value /= 10;
      ++result;
    }
  }
  else
  {
    while (value > 9)
    {
      value /= 10;
      ++result;
    }
  }

  return result;
}

static unsigned int uint_length_as_ascii(uint64_t value)
{
  unsigned int result = 1;
  while (value > 9)
  {
    value /= 10;
    ++result;
  }

  return result;
}

#if defined(TEST)
SPECIFICATION(string_ascii)
{
  heap_init();
  arena_init(&scratch_arena, 1024*1024);

  SCENARIO("String 'abc' has length 3")
  {
    CHECK(ascii_length("abc") == 3);
  }

  SCENARIO("Empty string has length 0")
  {
    CHECK(ascii_length("") == 0);
  }

  SCENARIO("Integer 6 converts to string \"6\"")
  {
    char* result = int_to_ascii(6);

    CHECK(result[0] == '6');
    CHECK(result[1] == '\0');
  }

  SCENARIO("Integer 572 converts to string \"572\"")
  {
    char* result = int_to_ascii(572);

    CHECK(result[0] == '5');
    CHECK(result[1] == '7');
    CHECK(result[2] == '2');
    CHECK(result[3] == '\0');
  }

  SCENARIO("Integer -4 converts to string \"-4\"")
  {
    char* result = int_to_ascii(-4);

    CHECK(result[0] == '-');
    CHECK(result[1] == '4');
    CHECK(result[2] == '\0');
  }

  SCENARIO("Integer -6328 converts to string \"-6328\"")
  {
    char* result = int_to_ascii(-6328);

    CHECK(result[0] == '-');
    CHECK(result[1] == '6');
    CHECK(result[2] == '3');
    CHECK(result[3] == '2');
    CHECK(result[4] == '8');
    CHECK(result[5] == '\0');
  }

  SCENARIO("Integer 0 converts to string \"0\"")
  {
    char* result = int_to_ascii(0);

    CHECK(result[0] == '0');
    CHECK(result[1] == '\0');
  }

  SCENARIO("Integer INT64_MAX converts to string \"9223372036854775807\"")
  {
    char* result = int_to_ascii(INT64_MAX);

    CHECK(result[0] == '9');
    CHECK(result[1] == '2');
    CHECK(result[2] == '2');
    CHECK(result[3] == '3');
    CHECK(result[4] == '3');
    CHECK(result[5] == '7');
    CHECK(result[6] == '2');
    CHECK(result[7] == '0');
    CHECK(result[8] == '3');
    CHECK(result[9] == '6');
    CHECK(result[10] == '8');
    CHECK(result[11] == '5');
    CHECK(result[12] == '4');
    CHECK(result[13] == '7');
    CHECK(result[14] == '7');
    CHECK(result[15] == '5');
    CHECK(result[16] == '8');
    CHECK(result[17] == '0');
    CHECK(result[18] == '7');
    CHECK(result[19] == '\0');
  }

  SCENARIO("Integer INT64_MIN converts to string \"-9223372036854775808\"")
  {
    char* result = int_to_ascii(INT64_MIN);

    CHECK(result[0] == '-');
    CHECK(result[1] == '9');
    CHECK(result[2] == '2');
    CHECK(result[3] == '2');
    CHECK(result[4] == '3');
    CHECK(result[5] == '3');
    CHECK(result[6] == '7');
    CHECK(result[7] == '2');
    CHECK(result[8] == '0');
    CHECK(result[9] == '3');
    CHECK(result[10] == '6');
    CHECK(result[11] == '8');
    CHECK(result[12] == '5');
    CHECK(result[13] == '4');
    CHECK(result[14] == '7');
    CHECK(result[15] == '7');
    CHECK(result[16] == '5');
    CHECK(result[17] == '8');
    CHECK(result[18] == '0');
    CHECK(result[19] == '8');
    CHECK(result[20] == '\0');
  }

  SCENARIO("String length of unsigned value 8254 is 4.")
  {
    int result = uint_length_as_ascii(8254);

    CHECK(result == 4);
  }

  SCENARIO("String length of unsigned value 7 is 1.")
  {
    int result = uint_length_as_ascii(7);

    CHECK(result == 1);
  }

  SCENARIO("String length of unsigned value 0 is 1.")
  {
    int result = uint_length_as_ascii(0);

    CHECK(result == 1);
  }

  SCENARIO("String length of an increasing unsigned value does not decrease.")
  {
    for (unsigned int i = 0; i < 1500; ++i)
    {
      int length = uint_length_as_ascii(i);
      int next_length = uint_length_as_ascii(i+1);

      CHECK(length <= next_length);
    }
  }

  SCENARIO("String length of signed value 8254 is 4.")
  {
    int result = int_length_as_ascii(8254);

    CHECK(result == 4);
  }

  SCENARIO("String length of signed value -8254 is 5.")
  {
    int result = int_length_as_ascii(-8254);

    CHECK(result == 5);
  }

  SCENARIO("String length of signed value 7 is 1.")
  {
    int result = int_length_as_ascii(7);

    CHECK(result == 1);
  }

  SCENARIO("String length of signed value -7 is 2.")
  {
    int result = int_length_as_ascii(-7);

    CHECK(result == 2);
  }

  SCENARIO("String length of signed value 0 is 1.")
  {
    int result = int_length_as_ascii(0);

    CHECK(result == 1);
  }

  SCENARIO("String length of an increasing positive signed value does not decrease.")
  {
    for (int i = 0; i < 1500; ++i)
    {
      int length = int_length_as_ascii(i);
      int next_length = int_length_as_ascii(i+1);

      CHECK(length <= next_length);
    }
  }

  SCENARIO("String length of a decreasing negative signed value does not decrease.")
  {
    for (int i = 0; i > -1500; --i)
    {
      int length = int_length_as_ascii(i);
      int next_length = int_length_as_ascii(i-1);

      CHECK(length <= next_length);
    }
  }

  arena_release(&scratch_arena);
  heap_release();
}

#endif

#endif
