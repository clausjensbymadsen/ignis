#ifndef COMMON_ASSERT_C
#define COMMON_ASSERT_C

#if !defined(DEBUG)
  #if defined(__GNUC__)
    #define assert_true(value)
    #define assert_false(value)
    #define assert_zero(value)
    #define assert_nonzero(value)
    #define assert_null(value)
    #define assert_nonnull(value)
    #define assert_positive(value)
    #define assert_nonnegative(value)
    #define assert_negative(value)
    #define assert_nonpositive(value)
    #define assert_unreachable() __builtin_unreachable()
  #elif defined(_MSC_VER)
    #define assert_true(value) __assume(value)
    #define assert_false(value) __assume(!(value))
    #define assert_zero(value) __assume((value) == 0)
    #define assert_nonzero(value) __assume((value) != 0)
    #define assert_null(value) __assume((value) == NULL)
    #define assert_nonnull(value) __assume((value) != NULL)
    #define assert_positive(value) __assume((value) > 0)
    #define assert_nonnegative(value) __assume((value) >= 0)
    #define assert_negative(value) __assume((value) < 0)
    #define assert_nonpositive(value) __assume((value) <= 0)
    #define assert_unreachable() __assume(0)
  #else
    #define assert_true(value)
    #define assert_false(value)
    #define assert_zero(value)
    #define assert_nonzero(value)
    #define assert_null(value)
    #define assert_nonnull(value)
    #define assert_positive(value)
    #define assert_nonnegative(value)
    #define assert_negative(value)
    #define assert_nonpositive(value)
    #define assert_unreachable()
  #endif
  #define not_implemented()
#else
  #define assert_true(value)        assert_impl((value),                 #value " is not true.",                    __FILE__, __LINE__)
  #define assert_false(value)       assert_impl(!(value),                #value " is not false.",                   __FILE__, __LINE__)
  #define assert_zero(value)        assert_impl(!(value),                #value " is not zero.",                    __FILE__, __LINE__)
  #define assert_nonzero(value)     assert_impl((value),                 #value " is not non-zero.",                __FILE__, __LINE__)
  #define assert_null(value)        assert_impl(!(value),                #value " is not NULL.",                    __FILE__, __LINE__)
  #define assert_nonnull(value)     assert_impl((value),                 #value " is not non-NULL.",                __FILE__, __LINE__)
  #define assert_positive(value)    assert_impl((value) > 0,             #value " is not positive.",                __FILE__, __LINE__)
  #define assert_nonpositive(value) assert_impl((value) <= 0,            #value " is not non-positive.",            __FILE__, __LINE__)
  #define assert_negative(value)    assert_impl((value) < 0,             #value " is not negative.",                __FILE__, __LINE__)
  #define assert_nonnegative(value) assert_impl((value) >= 0,            #value " is not non-negative.",            __FILE__, __LINE__)
  #define assert_unreachable()      assert_impl(0,                       "Unreachable execution path was reached.", __FILE__, __LINE__)
  #define not_implemented()         assert_impl(0,                       "This function has not been implemented.", __FILE__, __LINE__)

  #define assert_impl(assertion, message, file, line) do { if (!(assertion)) assert_report_failure(message, file, line); } while(0);
#endif

#if !defined(assert)
  #define assert(assertion) assert_true(assertion)
#endif
  
#endif
