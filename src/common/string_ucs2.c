#ifndef COMMON_STRING_UCS2_C
#define COMMON_STRING_UCS2_C

static size_t ucs2_length(const wchar_t* str) {
  assert_nonnull(str);
  
  size_t index = 0;
  while (str[index] != '\0') index++;
  return index;
}

#define INT_TO_UCS2_BUFFER_SIZE 30
static wchar_t* int_to_ucs2_buffer(int64_t value, wchar_t* buffer, unsigned int buffer_size) {
  assert_nonnull(buffer);

  buffer[buffer_size - 1] = L'\0';

  if (value == 0) {
    buffer[buffer_size - 2] = L'0';
    return &buffer[buffer_size - 2];
  }
  else if (value == INT64_MIN) {
    memory_copy(L"-9223372036854775808", buffer, 21 * sizeof(wchar_t)); // TODO: Handle corner case more elegantly
    return buffer;
  }
  else {
    int64_t abs_value = value < 0 ? -value : value;
    size_t i = buffer_size - 2;
    while (abs_value > 0) {
      buffer[i--] = (abs_value % 10) + '0';
      abs_value /= 10;
    }

    if (value < 0) buffer[i--] = '-';

    return &buffer[i+1];
  }
}

static inline wchar_t* int_to_ucs2(int64_t value)
{
  return int_to_ucs2_buffer(value, arena_allocate(&scratch_arena, INT_TO_UCS2_BUFFER_SIZE * sizeof(wchar_t)), INT_TO_UCS2_BUFFER_SIZE);
}

#define int_length_as_ucs2(value) int_to_ascii_buffer(value)
#define uint_length_as_ucs2(value) uint_to_ascii_buffer(value)

#if defined(TEST)
SPECIFICATION(string_ucs2)
{
  heap_init();
  arena_init(&scratch_arena, 1024*1024);

  SCENARIO("Wide string 'abc' has length 3")
  {
    CHECK(ucs2_length(L"abc") == 3);
  }

  SCENARIO("Empty wide string has length 0")
  {
    CHECK(ucs2_length(L"") == 0);
  }

  SCENARIO("Integer 6 converts to string \"6\"")
  {
    wchar_t* result = int_to_ucs2(6);

    CHECK(result[0] == L'6');
    CHECK(result[1] == L'\0');
  }

  SCENARIO("Integer 572 converts to string \"572\"")
  {
    wchar_t* result = int_to_ucs2(572);

    CHECK(result[0] == L'5');
    CHECK(result[1] == L'7');
    CHECK(result[2] == L'2');
    CHECK(result[3] == L'\0');
  }

  SCENARIO("Integer -4 converts to string \"-4\"")
  {
    wchar_t* result = int_to_ucs2(-4);

    CHECK(result[0] == L'-');
    CHECK(result[1] == L'4');
    CHECK(result[2] == L'\0');
  }

  SCENARIO("Integer -6328 converts to string \"-6328\"")
  {
    wchar_t* result = int_to_ucs2(-6328);

    CHECK(result[0] == L'-');
    CHECK(result[1] == L'6');
    CHECK(result[2] == L'3');
    CHECK(result[3] == L'2');
    CHECK(result[4] == L'8');
    CHECK(result[5] == L'\0');
  }

  SCENARIO("Integer 0 converts to string \"0\"")
  {
    wchar_t* result = int_to_ucs2(0);

    CHECK(result[0] == L'0');
    CHECK(result[1] == L'\0');
  }

  SCENARIO("Integer INT64_MAX converts to string \"9223372036854775807\"")
  {
    wchar_t* result = int_to_ucs2(INT64_MAX);

    CHECK(result[0] == L'9');
    CHECK(result[1] == L'2');
    CHECK(result[2] == L'2');
    CHECK(result[3] == L'3');
    CHECK(result[4] == L'3');
    CHECK(result[5] == L'7');
    CHECK(result[6] == L'2');
    CHECK(result[7] == L'0');
    CHECK(result[8] == L'3');
    CHECK(result[9] == L'6');
    CHECK(result[10] == L'8');
    CHECK(result[11] == L'5');
    CHECK(result[12] == L'4');
    CHECK(result[13] == L'7');
    CHECK(result[14] == L'7');
    CHECK(result[15] == L'5');
    CHECK(result[16] == L'8');
    CHECK(result[17] == L'0');
    CHECK(result[18] == L'7');
    CHECK(result[19] == L'\0');
  }

  SCENARIO("Integer INT64_MIN converts to string \"-9223372036854775808\"")
  {
    wchar_t* result = int_to_ucs2(INT64_MIN);

    CHECK(result[0] == L'-');
    CHECK(result[1] == L'9');
    CHECK(result[2] == L'2');
    CHECK(result[3] == L'2');
    CHECK(result[4] == L'3');
    CHECK(result[5] == L'3');
    CHECK(result[6] == L'7');
    CHECK(result[7] == L'2');
    CHECK(result[8] == L'0');
    CHECK(result[9] == L'3');
    CHECK(result[10] == L'6');
    CHECK(result[11] == L'8');
    CHECK(result[12] == L'5');
    CHECK(result[13] == L'4');
    CHECK(result[14] == L'7');
    CHECK(result[15] == L'7');
    CHECK(result[16] == L'5');
    CHECK(result[17] == L'8');
    CHECK(result[18] == L'0');
    CHECK(result[19] == L'8');
    CHECK(result[20] == L'\0');
  }

  arena_release(&scratch_arena);
  heap_release();
}
#endif

#endif
