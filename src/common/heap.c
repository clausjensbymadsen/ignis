#ifndef COMMON_HEAP_C
#define COMMON_HEAP_C

// Forward declarations implemented in corresponding platform-specific files.
static bool heap_init_impl(void);
static void heap_release_impl(void);
static void* heap_allocate_impl(size_t size);
static void* heap_allocate_zero_impl(size_t size);
static void* heap_reallocate_impl(void* allocation, size_t size);
static void* heap_reallocate_zero_impl(void* allocation, size_t size);
static void heap_deallocate_impl(void* allocation);

static inline unsigned int alignment_offset(msvc_in void* p, msvc_in unsigned int alignment)
{
  assert_nonnull(p);
  assert_positive(alignment);

  return (uintptr_t)p % alignment;
}

static inline bool aligned(msvc_in void* p, msvc_in unsigned int alignment)
{
  assert_nonnull(p);
  assert_positive(alignment);

  return alignment_offset(p, alignment) == 0;
}

static void* align(msvc_in char* p, msvc_in unsigned int alignment)
{
  assert_nonnull(p);
  assert_positive(alignment);

  unsigned int offset = alignment_offset(p, alignment);
  if (offset > 0) p = p - offset + alignment;

  assert_true(aligned(p, alignment));
  return p;
}

#if defined(DEBUG)
  // heap_initialized lets the heap allocation module check whether it's
  // initialized properly.
  bool heap_initialized = false;

  // heap_mock_failure enables testing to simulate allocation failure.
  // While true, every allocation call fails.
  bool heap_mock_failure = false;

  // In debug builds, every allocation is padded with particular values in
  // each end of the allocation to detect out-of-bounds writes.
  // HEAP_PADDING_SIZE and HEAP_PADDING_VALUE determines the size and values
  // of this padding.
  #define HEAP_PADDING_SIZE (512 / CHAR_BIT)
  #define HEAP_PADDING_VALUE 0x55

  // heap_entries is a resizable array of heap_entry structs containing
  // allocation information for debugging purposes like memory leak
  // detection. heap_entries_size is the number of elements currently
  // allocated for the array.
  struct heap_entry
  {
    void* p;
    size_t size;
    const char* file;
    size_t line;
  };
  typedef struct heap_entry heap_entry;

  heap_entry* heap_entries = NULL;
  size_t heap_entries_size = 0;
  #define HEAP_ENTRIES_SIZE_INCREMENT (4096 / sizeof(heap_entry))

  static bool heap_allocation_in_use(void* allocation)
  {
    assert_nonnull(allocation);

    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p == (char*)allocation - HEAP_PADDING_SIZE)
      {
        return true;
      }
    }
    return false;
  }

  static bool heap_leaking(void)
  {
    assert_true(heap_initialized);

    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p != NULL) return true;
    }

    return false;
  }

  // Checks whether the padding values of an allocation has been changed.
  static bool heap_allocation_violated(msvc_in void* allocation)
  {
    assert_nonnull(allocation);
    assert_true(heap_initialized);
    assert_true(heap_allocation_in_use(allocation));

    size_t size = 0;
    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p == (char*)allocation - HEAP_PADDING_SIZE)
      {
        size = heap_entries[i].size;
        break;
      }
    }
    assert_nonzero(size);

    for (int i = -HEAP_PADDING_SIZE; i < 0; ++i)
    {
      if (((char*)allocation)[i] != HEAP_PADDING_VALUE) return true;
    }
    for (size_t i = size; i < size + HEAP_PADDING_SIZE; ++i)
    {
      if (((char*)allocation)[i] != HEAP_PADDING_VALUE) return true;
    }

    return false;
  }

  static bool heap_init(void)
  {
    assert_false(heap_initialized);

    if (heap_mock_failure) return false;

    if (!heap_init_impl()) return false;

    #if defined(DEBUG)
      heap_initialized = true;
      assert_null(heap_entries);
      heap_entries = heap_allocate_zero_impl(HEAP_ENTRIES_SIZE_INCREMENT * sizeof(heap_entry));
      heap_entries_size = HEAP_ENTRIES_SIZE_INCREMENT;
    #endif

    return true;
  }

  static void heap_release(void)
  {
    assert_true(heap_initialized);

    #if defined(DEBUG)
      unsigned int leaks = 0;
      for (size_t i = 0; i < heap_entries_size; ++i)
      {
        if (heap_entries[i].p == NULL) continue;

        leaks++;
        printf("LEAK: 0x%p, %zu bytes, at %s:%zu\n", heap_entries[i].p, heap_entries[i].size, heap_entries[i].file, heap_entries[i].line);
      }
      if (leaks) printf("%u leaks.\n", leaks);
      assert_zero(leaks);

      heap_initialized = false;
      assert_nonnull(heap_entries);
      heap_deallocate_impl(heap_entries);
      heap_entries = NULL;
      heap_entries_size = 0;
    #endif

    heap_release_impl();
  }

  #define heap_allocate(size) heap_allocate_file_line((size), __FILE__, __LINE__)
  static void* heap_allocate_file_line(msvc_in size_t size, msvc_in const char* file, msvc_in size_t line)
  {
    assert_true(heap_initialized);
    assert_nonzero(size);

    if (heap_mock_failure) return NULL;
  
    char* p = heap_allocate_impl(HEAP_PADDING_SIZE + size + HEAP_PADDING_SIZE);
    if (!p) return NULL;

    assert_nonnull(heap_entries);
    assert_positive(heap_entries_size);
    heap_entry* entry = NULL;
    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p == NULL)
      {
        entry = &heap_entries[i];
        break;
      }
    }
    if (!entry)
    {
      heap_entry* new_entries = heap_reallocate_zero_impl(heap_entries, (heap_entries_size + HEAP_ENTRIES_SIZE_INCREMENT) * sizeof(heap_entry));
      if (!new_entries)
      {
        heap_deallocate_impl(p);
        return NULL;
      }
      heap_entries = new_entries;
      entry = &heap_entries[heap_entries_size];
      heap_entries_size += HEAP_ENTRIES_SIZE_INCREMENT;
    }
    assert_nonnull(entry);
    entry->p = p;
    entry->size = size;
    entry->file = file;
    entry->line = line;

    memory_set(p, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);
    memory_set(p + HEAP_PADDING_SIZE + size, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);

    assert_true(heap_allocation_in_use(p + HEAP_PADDING_SIZE));
    return p + HEAP_PADDING_SIZE;
  }
  
  #define heap_allocate_zero(size) heap_allocate_zero_file_line((size), __FILE__, __LINE__)
  static void* heap_allocate_zero_file_line(msvc_in size_t size, msvc_in const char* file, msvc_in size_t line)
  {
    assert_true(heap_initialized);
    assert_nonzero(size);
  
    if (heap_mock_failure) return NULL;

    char* p = heap_allocate_zero_impl(HEAP_PADDING_SIZE + size + HEAP_PADDING_SIZE);
    if (!p) return NULL;

    assert_nonnull(heap_entries);
    assert_positive(heap_entries_size);
    heap_entry* entry = NULL;
    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p == NULL)
      {
        entry = &heap_entries[i];
        break;
      }
    }
    if (!entry)
    {
      heap_entry* new_entries = heap_reallocate_zero_impl(heap_entries, (heap_entries_size + HEAP_ENTRIES_SIZE_INCREMENT) * sizeof(heap_entry));
      memory_set_zero((char*)&heap_entries[heap_entries_size], HEAP_ENTRIES_SIZE_INCREMENT * sizeof(heap_entry));
      if (!new_entries)
      {
        heap_deallocate_impl(p);
        return NULL;
      }
      heap_entries = new_entries;
      entry = &heap_entries[heap_entries_size];
      heap_entries_size += HEAP_ENTRIES_SIZE_INCREMENT;
    }
    assert_nonnull(entry);
    entry->p = p;
    entry->size = size;
    entry->file = file;
    entry->line = line;

    memory_set(p, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);
    memory_set(p + HEAP_PADDING_SIZE + size, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);

    assert_true(heap_allocation_in_use(p + HEAP_PADDING_SIZE));
    return p + HEAP_PADDING_SIZE;
  }
  
  #define heap_reallocate(allocation, size) heap_reallocate_file_line((allocation), (size), __FILE__, __LINE__)
  static void* heap_reallocate_file_line(msvc_in void* allocation, msvc_in size_t size, msvc_in const char* file, msvc_in size_t line)
  {
    assert_true(heap_initialized);
    assert_nonzero(size);
    assert_true(heap_allocation_in_use(allocation));
    assert_false(heap_allocation_violated(allocation));
  
    if (heap_mock_failure) return NULL;

    void* new_allocation = heap_reallocate_impl(((char*)allocation - HEAP_PADDING_SIZE), HEAP_PADDING_SIZE + size + HEAP_PADDING_SIZE);

    if (new_allocation)
    {
      assert_nonnull(heap_entries);
      assert_positive(heap_entries_size);
      for (size_t i = 0; i < heap_entries_size; ++i)
      {
        if (heap_entries[i].p == allocation)
        {
          heap_entries[i].p = new_allocation;
          heap_entries[i].size = size;
          heap_entries[i].file = file;
          heap_entries[i].line = line;
          break;
        }
      }
    }

    memory_set((char*)new_allocation + HEAP_PADDING_SIZE + size, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);

    assert_true(heap_allocation_in_use((char*)new_allocation + HEAP_PADDING_SIZE));
    return (char*)new_allocation + HEAP_PADDING_SIZE;
  }
  
  #define heap_reallocate_zero(allocation, size) heap_reallocate_zero_file_line((allocation), (size), __FILE__, __LINE__)
  static void* heap_reallocate_zero_file_line(msvc_in void* allocation, msvc_in size_t size, msvc_in const char* file, msvc_in size_t line)
  {
    assert_true(heap_initialized);
    assert_nonzero(size);
    assert_true(heap_allocation_in_use(allocation));
    assert_false(heap_allocation_violated(allocation));
  
    if (heap_mock_failure) return NULL;

    void* new_allocation = heap_reallocate_zero_impl(((char*)allocation - HEAP_PADDING_SIZE), HEAP_PADDING_SIZE + size + HEAP_PADDING_SIZE);

    if (new_allocation)
    {
      assert_nonnull(heap_entries);
      assert_positive(heap_entries_size);
      for (size_t i = 0; i < heap_entries_size; ++i)
      {
        if (heap_entries[i].p == allocation)
        {
          heap_entries[i].p = new_allocation;
          heap_entries[i].size = size;
          heap_entries[i].file = file;
          heap_entries[i].line = line;
          break;
        }
      }
    }

    memory_set((char*)new_allocation + HEAP_PADDING_SIZE + size, HEAP_PADDING_SIZE, HEAP_PADDING_VALUE);

    assert_true(heap_allocation_in_use((char*)new_allocation + HEAP_PADDING_SIZE));
    return (char*)new_allocation + HEAP_PADDING_SIZE;
  }
  
  static void heap_deallocate(msvc_in void* allocation)
  {
    assert_true(heap_initialized);
    assert_nonnull(allocation);
    assert_true(heap_allocation_in_use(allocation));
    assert_false(heap_allocation_violated(allocation));

    assert_nonnull(heap_entries);
    assert_positive(heap_entries_size);
    for (size_t i = 0; i < heap_entries_size; ++i)
    {
      if (heap_entries[i].p == (char*)allocation - HEAP_PADDING_SIZE)
      {
        heap_entries[i].p = NULL;
        break;
      }
    }
    
    heap_deallocate_impl((char*)allocation - HEAP_PADDING_SIZE);
  }

#else
  #define heap_init(size)                   heap_init_impl(size)
  #define heap_release(size)                heap_release_impl(size)
  #define heap_allocate(size)               heap_allocate_impl(size)
  #define heap_allocate_zero(size)          heap_allocate_zero_impl(size)
  #define heap_reallocate(alloc, size)      heap_reallocate_impl((alloc), (size))
  #define heap_reallocate_zero(alloc, size) heap_reallocate_zero_impl((alloc), (size))
  #define heap_deallocate(size)             heap_deallocate_impl(size)
#endif

#if defined(TEST)
SPECIFICATION(heap)
{
  heap_init();

  SCENARIO("Allocating heap memory returns a pointer.")
  {
    void* ptr = heap_allocate(10);

    CHECK(ptr != NULL);
    CHECK(!heap_allocation_violated(ptr));

    heap_deallocate(ptr);
  }
  
  SCENARIO("Heap reallocating to a bigger size preserves the contained data.")
  {
    char* data = heap_allocate(3);
    CHECK(data != NULL);
    data[0] = 'a';
    data[1] = 'b';
    data[2] = 'c';

    char* new_data = heap_reallocate(data, 5);

    CHECK(new_data != NULL);
    CHECK(new_data[0] == 'a');
    CHECK(new_data[1] == 'b');
    CHECK(new_data[2] == 'c');
    CHECK(!heap_allocation_violated(new_data));

    if (new_data)
      heap_deallocate(new_data);
    else
      heap_deallocate(data);
  }

  SCENARIO("Heap reallocating to a smaller size truncates the contained data.")
  {
    char* data = heap_allocate(5);
    CHECK(data != NULL);
    data[0] = 'a';
    data[1] = 'b';
    data[2] = 'c';
    data[3] = 'd';
    data[4] = 'e';

    char* new_data = heap_reallocate(data, 3);
    CHECK(!heap_allocation_violated(new_data));

    CHECK(new_data != NULL);
    CHECK(new_data[0] == 'a');
    CHECK(new_data[1] == 'b');
    CHECK(new_data[2] == 'c');

    if (new_data)
      heap_deallocate(new_data);
    else
      heap_deallocate(data);
  }

  SCENARIO("1 allocation and 1 deallocation does not leak")
  {
    char* data = heap_allocate(10);
    heap_deallocate(data);

    CHECK(!heap_leaking());
  }

  SCENARIO("2 allocations and 1 deallocation does leak")
  {
    char* data1 = heap_allocate(10);
    char* data2 = heap_allocate(15);
    heap_deallocate(data1);

    CHECK(heap_leaking());

    heap_deallocate(data2);
  }

  SCENARIO("1 allocation, 1 reallocation and 1 deallocation does not leak")
  {
    char* data = heap_allocate(10);
    char* new_data = heap_reallocate(data, 20);
    if (new_data) data = new_data;
    heap_deallocate(data);

    CHECK(!heap_leaking());
  }
  
  SCENARIO("Allocating 10 bytes and setting all 10 bytes does not violate the allocation boundary.")
  {
    char* data = heap_allocate(10);
    for (int i = 0; i < 10; ++i) data[i] = !HEAP_PADDING_VALUE;

    CHECK(!heap_allocation_violated(data));

    heap_deallocate(data);
  }

  SCENARIO("Allocating 10 bytes and setting 11 bytes from index -1 to 9 underflows.")
  {
    char* data = heap_allocate(10);
    for (int i = -1; i < 10; ++i) data[i] = !HEAP_PADDING_VALUE;

    CHECK(heap_allocation_violated(data));

    data[-1] = HEAP_PADDING_VALUE;
    heap_deallocate(data);
  }

  SCENARIO("Allocating 10 bytes and setting 11 bytes from index 0 to 10 overflows.")
  {
    char* data = heap_allocate(10);
    for (int i = 0; i < 11; ++i) data[i] = !HEAP_PADDING_VALUE;

    CHECK(heap_allocation_violated(data));

    data[10] = HEAP_PADDING_VALUE;
    heap_deallocate(data);
  }

  heap_release();
}
#endif

#endif
