#ifndef COMMON_STRING_UTF8_C
#define COMMON_STRING_UTF8_C

static size_t utf8_length(const char* str)
{
  assert_nonnull(str);

  not_implemented();
  return 0;  
}

#define INT_TO_UTF8_BUFFER_SIZE 30
#define int_to_utf8_buffer(value, buffer, buffer_size) int_to_ascii_buffer((value), (buffer), (buffer_size))

static inline char* int_to_utf8(int64_t value)
{
  return int_to_utf8_buffer(value, arena_allocate(&scratch_arena, INT_TO_UTF8_BUFFER_SIZE), INT_TO_UTF8_BUFFER_SIZE);
}

#define int_length_as_utf8(value) int_to_ascii_buffer(value)
#define uint_length_as_utf8(value) uint_to_ascii_buffer(value)

#endif
