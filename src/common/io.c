#ifndef COMMON_IO_C
#define COMMON_IO_C

// Forward declarations implemented in corresponding platform-specific files.
static void* load_file_into_memory_str(const char* filename);
static void* load_file_into_memory_wstr(const wchar_t* filename);

#define load_file_into_memory(filename) \
  _Generic(filename, \
    char*: load_file_into_memory_str, \
    const char*: load_file_into_memory_str, \
    wchar_t*: load_file_into_memory_wstr, \
    const wchar_t*: load_file_into_memory_wstr \
  )(filename)

#endif