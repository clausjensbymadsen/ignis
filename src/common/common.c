#ifndef COMMON_COMMON_C
#define COMMON_COMMON_C

#if defined(__cplusplus)
  #error The project must be compiled as C11 or newer.
#endif

#if !defined(_MSC_VER) && !defined(__STDC__)
  #error The project must be compiled as C11 or newer.
#endif

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ < 201112L)
  #error The project must be compiled as C11 or newer.
#endif

#if !defined(__AVX2__)
  #if defined(__GNUC__)
    #error Compile with the -mavx2 option
  #endif
#endif

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

// Workaround for lack of C11 support in MSVC
#if defined(_MSC_VER)
  #define alignof _Alignof
  #define alignas(alignment) __declspec(align(alignment))
#else
  #include <stdalign.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#if defined(_WIN32)
  #if defined(_MSC_VER)
    #pragma warning(push, 0)
  #endif
  #define UNICODE
  #include <windows.h>
  #include <commctrl.h>
  #if defined(_MSC_VER)
    // MinGW does not have this header.
    #include <shobjidl_core.h>
  #endif
  #if defined(_MSC_VER)
    #pragma warning(pop)
  #endif
#endif

#define CONCATENATE_IMPL(x, y) x##y
#define CONCATENATE(x, y) CONCATENATE_IMPL(x, y)

#define STRINGIZE(x) #x 

#endif
