#ifndef COMMON_STRING_BUILDER_C
#define COMMON_STRING_BUILDER_C

void* string_builder_append_ch(char ch)
{
  char* result = arena_allocate(&scratch_arena, 1);
  *result = ch;
  return result;
}

void* string_builder_append_wch(wchar_t ch)
{
  wchar_t* result = arena_allocate(&scratch_arena, sizeof(wchar_t));
  *result = ch;
  return result;
}

void* string_builder_append_ascii(const char* str)
{
  size_t length = ascii_length(str);
  char* result = arena_allocate(&scratch_arena, length);

  memory_copy(str, result, length);

  return result;
}

void* string_builder_append_ucs2(const wchar_t* str)
{
  size_t length = ucs2_length(str);
  char* result = arena_allocate(&scratch_arena, sizeof(wchar_t) * length);

  memory_copy(str, result, sizeof(wchar_t) * length);

  return result;
}

void* string_builder_append_int_ascii(int64_t value)
{
  unsigned int length = int_length_as_ascii(value);
  char* result = arena_allocate(&scratch_arena, length+1);
  arena_usage_decrease(&scratch_arena, 1);
  int_to_ascii_buffer(value, result, length+1);

  return result;
}

void* string_builder_append_uint_ascii(uint64_t value)
{
  unsigned int length = uint_length_as_ascii(value);
  char* result = arena_allocate(&scratch_arena, length+1);
  arena_usage_decrease(&scratch_arena, 1);
  uint_to_ascii_buffer(value, result, length+1);

  return result;
}

#define string_builder_append(arg) _Generic((arg), \
  char:           string_builder_append_ch,        \
  wchar_t:        string_builder_append_wch,       \
  char*:          string_builder_append_ascii,     \
  const char*:    string_builder_append_ascii,     \
  wchar_t*:       string_builder_append_ucs2,      \
  const wchar_t*: string_builder_append_ucs2       \
  )(arg)

#if defined(TEST)
SPECIFICATION(string_builder)
{
  heap_init();
  arena_init(&scratch_arena, 1024*1024);

  SCENARIO("Adding narrow character 'a' yields 'a'.")
  {
    char* result = string_builder_append_ch('a');

    CHECK(result[0] == 'a');
  }

  SCENARIO("Adding wide character 'a' yields 'a'.")
  {
    wchar_t* result = string_builder_append_wch(L'a');

    CHECK(result[0] == L'a');
  }

  SCENARIO("Adding narrow character 'd' to narrow string 'abc' yields 'abcd'.")
  {
    char* result = string_builder_append_ascii("abc");
    string_builder_append_ch('d');

    CHECK(result[0] == 'a');
    CHECK(result[1] == 'b');
    CHECK(result[2] == 'c');
    CHECK(result[3] == 'd');
  }

  SCENARIO("Adding wide character 'd' to wide string 'abc' yields 'abcd'.")
  {
    wchar_t* result = string_builder_append_ucs2(L"abc");
    string_builder_append_wch(L'd');

    CHECK(result[0] == L'a');
    CHECK(result[1] == L'b');
    CHECK(result[2] == L'c');
    CHECK(result[3] == L'd');
  }

  SCENARIO("Adding narrow string 'abc' yields 'abc'.")
  {
    char* result = string_builder_append_ascii("abc");

    CHECK(result[0] == 'a');
    CHECK(result[1] == 'b');
    CHECK(result[2] == 'c');
  }

  SCENARIO("Adding narrow strings 'abc' and 'def' yields narrow string 'abcdef'.")
  {
    char* result = string_builder_append_ascii("abc");
    string_builder_append_ascii("def");

    CHECK(result[0] == 'a');
    CHECK(result[1] == 'b');
    CHECK(result[2] == 'c');
    CHECK(result[3] == 'd');
    CHECK(result[4] == 'e');
    CHECK(result[5] == 'f');
  }

  SCENARIO("Adding wide string 'abc' yields 'abc'.")
  {
    wchar_t* result = string_builder_append_ucs2(L"abc");

    CHECK(result[0] == L'a');
    CHECK(result[1] == L'b');
    CHECK(result[2] == L'c');
  }

  SCENARIO("Adding wide strings 'abc' and 'def' yields wide string 'abcdef'.")
  {
    wchar_t* result = string_builder_append_ucs2(L"abc");
    string_builder_append_ucs2(L"def");

    CHECK(result[0] == L'a');
    CHECK(result[1] == L'b');
    CHECK(result[2] == L'c');
    CHECK(result[3] == L'd');
    CHECK(result[4] == L'e');
    CHECK(result[5] == L'f');
  }

  SCENARIO("Adding narrow string 'abc', value 123 and narrow string 'def' yields 'abc123def'.")
  {
    char* result = string_builder_append_ascii("abc");
    string_builder_append_int_ascii(123);
    string_builder_append_ascii("def");

    CHECK(result[0] == 'a');
    CHECK(result[1] == 'b');
    CHECK(result[2] == 'c');
    CHECK(result[3] == '1');
    CHECK(result[4] == '2');
    CHECK(result[5] == '3');
    CHECK(result[6] == 'd');
    CHECK(result[7] == 'e');
    CHECK(result[8] == 'f');
  }

  SCENARIO("Adding narrow string 'abc', value -456 and narrow string 'def' yields 'abc123def'.")
  {
    char* result = string_builder_append_ascii("abc");
    string_builder_append_int_ascii(-456);
    string_builder_append_ascii("def");

    CHECK(result[0] == 'a');
    CHECK(result[1] == 'b');
    CHECK(result[2] == 'c');
    CHECK(result[3] == '-');
    CHECK(result[4] == '4');
    CHECK(result[5] == '5');
    CHECK(result[6] == '6');
    CHECK(result[7] == 'd');
    CHECK(result[8] == 'e');
    CHECK(result[9] == 'f');
  }

  arena_release(&scratch_arena);
  heap_release();
}
#endif

#endif
