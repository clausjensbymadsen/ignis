#ifndef COMMON_MEMORY_C
#define COMMON_MEMORY_C

#if !defined(NO_BUILTINS)
  #define memory_set(destination, length, value) memset(destination, value, length)
  #define memory_set_zero(destination, length)   memset(destination, 0, length)
  #define memory_copy(source, destination, size) memcpy(destination, source, size)
  #define memory_move(source, destination, size) memmove(destination, source, size)
  #define memory_compare(ptr1, ptr2, size)       (memcmp(ptr1, ptr2, size) == 0)
#else
  #error Builtin replacements not implemented.
#endif

#if defined(TEST)
SPECIFICATION(memory)
{
}
#endif

#endif
