#ifndef COMMON_EMIT_C
#define COMMON_EMIT_C

#if defined(DEBUG) || defined(BENCHMARK)
  #define emit_base(...) fprintf(stderr, __VA_ARGS__)

  #define emit_info(...)    do { emit_base("INFO:    "); emit_base("%s:%d: ", __FILE__, __LINE__); emit_base(__VA_ARGS__); emit_base("\n"); } while (0)
  #define emit_warning(...) do { emit_base("WARNING: "); emit_base("%s:%d: ", __FILE__, __LINE__); emit_base(__VA_ARGS__); emit_base("\n"); } while (0)
  #define emit_error(...)   do { emit_base("ERROR:   "); emit_base("%s:%d: ", __FILE__, __LINE__); emit_base(__VA_ARGS__); emit_base("\n"); } while (0)
#else
  #define emit_info(...)
  #define emit_warning(...)
  #define emit_error(...)
#endif

#endif
