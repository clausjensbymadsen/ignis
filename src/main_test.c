#define TEST
#define DEBUG

// Enable workaround for the ancient Microsoft C Runtime not supporting C99.
#if defined(__MINGW32__) || defined(__MINGW64__)
  #if !defined(__USE_MINGW_ANSI_STDIO)
    #define __USE_MINGW_ANSI_STDIO 1
  #endif
#endif

#include "common/common.c"
#include "common/annotation.c"
#include "common/emit.c"

size_t scenarios = 0;
size_t failures = 0;
const char* current_specification = NULL;
bool current_specification_printed = false;
const char* current_scenario = NULL;

#define SPECIFICATION(name) \
  static void CONCATENATE(test_impl_, name)(void); \
  static void CONCATENATE(test_, name)(void) \
  { \
    current_specification = #name; \
    current_specification_printed = false; \
    CONCATENATE(test_impl_, name)(); \
  } \
  static void CONCATENATE(test_impl_, name)(void)

#define SCENARIO(name) \
  do \
  { \
    scenarios++; \
    current_scenario = name; \
  } while(0);

#define CHECK(assertion) \
  do \
  { \
    if (!(assertion)) \
    { \
      failures++; \
      if (!current_specification_printed) \
      { \
        printf("%s\n", current_specification); \
        current_specification_printed = true; \
      } \
      printf("  "); \
      color_highlight(); \
      printf("%s", current_scenario); \
      color_default(); \
      printf(" [ %s ]\n", #assertion); \
    } \
  } while (0);

static inline void color_default()
{
  #if defined(_WIN32)
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
  #endif
}

static inline void color_highlight()
{
  #if defined(_WIN32)
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
  #endif
}

static inline void color_green()
{
  #if defined(_WIN32)
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);
  #endif
}

static inline void color_red()
{
  #if defined(_WIN32)
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);
  #endif
}

#if defined(_WIN32)
  #include "view/win32/assert.c"
  #include "common/memory.c"
  #include "view/win32/heap.c"
#else
  #include "std/assert.c"
  #include "common/memory.c"
  #include "std/heap.c"
#endif
#include "common/arena.c"
#include "common/string_ascii.c"
#include "common/string_ucs2.c"
#include "common/string_utf8.c"
#include "common/string_builder.c"
#include "common/reader.c"

#include "model/model.c"

int main(void)
{
  printf("Running tests...\n");

  test_memory();
  test_heap();
  test_arena();
  test_image();
  test_layer();
  test_image_edit();
  test_string_ascii();
  test_string_ucs2();
  test_string_builder();
  test_reader();

  if (failures == 0)
  {
    color_green();
    printf("0/%zu failed scenarios\n", scenarios);
  }
  else
  {
    color_red();
    printf("%zu/%zu failed scenarios\n", failures, scenarios);
  }
  color_default();

  return EXIT_SUCCESS;
}
