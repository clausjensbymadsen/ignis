# Contributing to Ignis
Ignis is not open to code contributions at the moment. When the project is
mature enough, this will change.

Other kinds of contribution are welcome though, like bug reports and feature
requests.

## Style guide
The project uses Allman style indentation with 2 spaces per level.

Identifiers and comments are in English.

Identifiers uses snake case.

## Directories
This is a brief description of the purpose of each directory of the project:

| Directory        | Description                                                                    |
| ---------------- | ------------------------------------------------------------------------------ |
| `src/common`     | Contains foundational code like memory management and string operations.       |
| `src/model`      | Contains the back-end of Ignis. The user interfaces make calls into this code. |
| `src/view`       | Contains platform-specific front-ends of Ignis.                                |
| `src/view/win32` | Contains code specific to the Windows version of Ignis.                        |
| `src/std`        | Contains standard library-dependent code.                                      |

## Architecture
This is a high level overview of the architecture of Ignis.

### Model and views
Ignis consist of a model (the back-end) and a number of views (front-ends),
one for each platform.

The model is responsible for holding the images in memory and to implement
the interface for editing the images. The model is platform-independent.

Each supported platform has a view that provides a user interface specific
for that platform. The view makes calls to the interface of the model to have
the requested operations performed. Views are supposed to do as little work
as possible, letting the model do as much of the task as possible.

Currently only a Win32-based view is implemented.

### Event-sourced system
The model is an event-sourced system - that is, instead of storing the state
of the image, it record the series of actions performed on the image to reach
this state. Examples of this are version control systems and blockchains.

This makes undo and redo trivial to implement, because any earlier state of
the image can be recreated at will.
